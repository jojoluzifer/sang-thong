<?php ob_start(); ?>
<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>.:: บริษัทขอนแก่นแสงทองอะไหล่ ::.</title>

        <link href="css/reset.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">

        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>  
        <script type="text/javascript" src="js/bootstrap.min.js"></script>  

    </head>
    <body>

        <div id="wrapper" >

            <div id="header">
                <?php include('template/sub-header.php') ?>
            </div>
            <div id="main">
                <div style="padding: 30px 0;">
                    <h1 style="text-align: center;text-decoration: underline;">สินค้า (Products)</h1>
                </div>

                <div class="product-menu">
                    <ul>
                        <li><a href="" >อะไหล่แท้</a></li>
                        <li><a href="" >อะไหล่ทดแทน</a></li>
                        <li><a href="" >น้ำมันเครื่อง/เบรค</a></li>
                        <li><a href="" >อะไหล่ช่วงล่าง</a></li>
                        <li><a href="" >เครื่องยนต์</a></li>
                        <li><a href="" >แบตเตอรี่</a></li>
                        <li><a href="" >เครื่องมือช่างอื่นๆ</a></li>
                    </ul>
                </div>

                <div class="main-products">
                    <div style="width: 95%;margin: 0 auto;">
                        <h2>อะไหล่แท้</h1>
                            <img src="img/true-autopart.png" />
                    </div>
                </div>
            </div>

        </div>



    </body>
</html>