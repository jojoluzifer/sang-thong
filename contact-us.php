<?php ob_start(); ?>
<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>.:: บริษัทขอนแก่นแสงทองอะไหล่ ::.</title>

        <link href="css/reset.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">

        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>  
        <script type="text/javascript" src="js/bootstrap.min.js"></script>  

    </head>
    <body>

        <div id="wrapper" >

            <div id="header">
                <?php include('template/sub-header.php') ?>
            </div>
            <div id="main">
                <div style="padding: 30px 0;">
                    <h1 style="text-align: center;text-decoration: underline;">เกี่ยวกับบริษัท (About us)</h1>
                </div>

                <div class="main-contact-us">
                    <p class="contact-us-t1">บริษัท ขอนแก่นแสงทองอะไหล่ [2007] จำกัด</p>
                    <p class="contact-us-t1">KHONKAEN SANGTHONG AUTOPART [2007] CO. , LTD</p>
                    <p class="contact-us-t2">160-164 ถ.หน้าเมือง ต.ในเมือง อ.เมือง จ.ขอนแก่น 40000</p>
                    <p class="contact-us-t2">โทรศัพท์ 043-243550, 043-247444 Fax 043-247555</p>
                    <p class="contact-us-t3">เปิดบริการทุกวัน !!</p>
                    <p ><span class="contact-us-t4">วันจันทร์-เสาร์ 08.00-17.30 น.</span><span class="contact-us-t5">&nbsp; &nbsp;&nbsp;วันอาทิตย์ 09.00-15.30 น.</span></p>
                   
                </div>
            </div>

        </div>



    </body>
</html>