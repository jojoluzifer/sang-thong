<div class="head-menu">
    <ul >
        <li>
            <ul>
                <li style="display: block;">
                    <a href="index.php" style="display: block;">
                        <p>หน้าหลัก </p>
                        <p> Home </p>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <ul>
                <li style="display: block;">
                    <a href="all-products.php" style="display: block;">
                        <p>สินค้า </p>
                        <p> Products </p>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <ul>
                <li style="display: block;">
                    <a href="about-us.php" style="display: block;">
                        <p>เกี่ยวกับบริษัท </p>
                        <p> About Us </p>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <ul>
                <li style="display: block;">
                    <a href="" style="display: block;">
                        <p>ผังองค์กร </p>
                        <p> Organization Chart.</p>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <ul>
                <li style="display: block;">
                    <a href="contact-us.php" style="display: block;">
                        <p>ติดต่อเรา </p>
                        <p>Contact Us</p>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>