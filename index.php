<?php ob_start(); ?>
<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>


        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>.:: บริษัทขอนแก่นแสงทองอะไหล่ ::.</title>

        <link href="css/reset.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">

        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>  
        <script type="text/javascript" src="js/bootstrap.min.js"></script>  

    </head>
    <body>

        <div id="wrapper" >

            <div id="header">
                <?php
                include('template/main-header.php');
                ?>
            </div>

            <div id="navigation">
                <div class="row-fluid">
                    <div class="span4">
                        <div class="welcome-text">
                            <img src="img/welcome-text.png" />
                        </div>
                    </div>
                    <div class="span8">

                        <?php include('template/navigation.php'); ?>
                    </div>


                </div>
            </div>
            <div id="main">

                <img class="main-pic" src="img/main-pic.png" />
                <img class="main-pic" style="margin-top: 50px" src="img/brand.png" />
            </div>

        </div>



    </body>
</html>