<?php ob_start(); ?>
<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>.:: บริษัทขอนแก่นแสงทองอะไหล่ ::.</title>

        <link href="css/reset.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">

        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>  
        <script type="text/javascript" src="js/bootstrap.min.js"></script>  

    </head>
    <body>

        <div id="wrapper" >

            <div id="header">
                <?php include('template/sub-header.php') ?>
            </div>
            <div id="main">
                <div style="padding: 30px 0;">
                    <h1 style="text-align: center;text-decoration: underline;">เกี่ยวกับบริษัท (About us)</h1>
                </div>

                <div class="main-about-us">

                    <p class="main-about-us-h1">ด้วยประสบการณ์กว่า 40 ปีในด้านจำหน่ายอะไหล่รถ</p>
                    <p>บริษัท ขอนแก่นแสงทองอะไหล่ [2007] จำกัด เป็นศูนย์รวมอะไหล่ และรถบรรทุก หลากหลายประเภท</p>
                    <p>บริษัทเราจัดจำหน่ายรถยนต์ รถบรรทุก หลากหลายยี่ห้อ อาทิเช่น TOYOTA, ISUZU, FORD, MITSUBISHI, NISSAN, MAZDA</p>
                    <p>รวมทั้งอะไหล่เครื่องจักรกลหนัก อาทิเช่น JOHNDEER, HINO, UD, NEW HOLLAND</p>
                    <p>ทั้งของแท้ และอะไหล่สั่งจากญี่ปุ่นซึ่งเทียบเท่ากับของแท้ติดรถ อาทิเช่น Seiken Nok Doldo Akemono Denzo NPR 555 เป็นต้น</p>
                    <p class="main-about-us-h1">สินค้าเรามีดังนี้...</p>
                    <p>อะไหล่ชุดยกเครื่อง , อะไหล่ช่วงล่าง , ผ้าใบคลุมรถ อุลตร้า - เอ็กตร้าแมทาริก , แบตเตอรี่รถยนต์</p>
                    <p>ลูกหมากขันส่ง-คันชัก-คันส่งกลาง-แร็ค-ปีกนก-กันโคลง , ชุดลูกสูบ-แหวน-ปลอกสูบ</p>
                    <p>ขาพอก-ก้าน-กันรุน-ราวลิ้น , ปะเก็นชุดยกเครื่อง , ผ้าเบรค , ลูกหมากล้อ , เฟืองเกียร์</p>
                    <p>แหนบหน้า-หลัง , หม้อลมเบรค , ตุ๊กตาเพลาบุ๊กกี้ , จานคลัช , จานกดครัช , ชุดซ่อมคลัชบน-ล่าง</p>
                    <p>มู่เลย์ไฟวิน , จานเฟืองเดือยหมู , บู๊ซก้านสูบ , วาล์วไอดี-ไอเสีย , ปะเก็นฝาสูบ , จานดิสเบรคหน้า-หลัง</p>
                    <p>ก้านเบครหน้า-หลัง , แม่ปั๊มเบรค , แม่ปั๊มคลัทซ์ , กระบอกเบรค , ชุดซ่อมแม่ปั๊มเบรค , ชุดซ่อมแม่ปั๊มคลัทซ์</p>
                    <p>ไดชาร์ต , ไดสตาร์ท , ยานแท่นเครื่อง , ยางแท่นเกียร์ , ชีลกันน้ำมัน , น้ำมันเครื่อง , น้ำมันเกียร์</p>
                    <p>ไส้กรองน้ำมันเครื่อง-อากาศ-โซล่า , อุปกรณ์ต่อพ่วง , อุปกรณ์ทั่วไป และ อื่นๆอีกมากมาย</p>
                </div>
            </div>

        </div>



    </body>
</html>